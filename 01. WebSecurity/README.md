
요약
---
- MySQL
- 기본 회원가입 기능 (가입, 인증, 로그인) + WebSecurity
- Mailjet 을 통한 메일링 서비스도 구현 되어있음

/signup 에서 회원 가입 가능 (이메일 발송까지 확인 가능)
/login 에서 로그인 -> 성공시 /login_sucess 로 이동<br>
(정상처리되었을 경우 현재 로그인된 유저 데이터가 화면에 출력 됨)

* * *

mysql 기본세팅 (application.properties 에서 수정가능)
--- 
- database: demo
- user: root / 1234

