package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.example.demo.user.LoginFailureHandler;
import com.example.demo.user.LoginSuccessHandler;
import com.example.demo.user.services.KewUserDetailsService;

@Configuration
public class SecurityConfiguration {
	
	@Configuration
	@EnableWebSecurity
	protected static class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {
		
		@Value("${spring.serverAddress}")
		String serverAddress;
		@Value("${spring.rememberMeDomain}")
		String rememberMeDomain;

		@Autowired
		KewUserDetailsService userDetailsService;
		@Autowired
		LoginSuccessHandler successHandler;
		@Autowired
		LoginFailureHandler failureHandler;

		/*
		@Override
		public void configure(WebSecurity web) throws Exception {
			web.ignoring()
				.antMatchers("/lib/**")
				.antMatchers("/kewtea/css/**")
				.antMatchers("/kewtea/lib/**")
				.antMatchers("/kewtea/img/**")
				.antMatchers("/kewtea/video/**")
				.antMatchers("/journal/img/**")
				.antMatchers("/journal/lib/**")
				.antMatchers("/journal/js/**")
				.antMatchers("/journal/css/**")
				.antMatchers("/journal/product/**")
				.antMatchers("/naim/css/**")
				.antMatchers("/naim/js/**")
				.antMatchers("/naim/img/**")
				.antMatchers("/naim/extensions/**")
				.antMatchers("/naim/player/css/**")
				.antMatchers("/naim/player/js/**");
		}
		*/
		
		@Override
		public void configure(HttpSecurity http) throws Exception {

			// cross domain problem
			http.csrf().disable();

			http.authorizeRequests()
					.anyRequest().permitAll(); // 모든 http 에 header 값에 인증 정보를 실어서 보내도록 함 (막는 것이 아님)

			http.formLogin()
					.loginPage("/login")
					.loginProcessingUrl("/dologin")
					.usernameParameter("username").passwordParameter("password")
					.successHandler(successHandler)
					.failureHandler(failureHandler)
					.permitAll();

			http.rememberMe()
					.key("rem-me-key")
		            .rememberMeParameter("remember-me-param")
		            .rememberMeCookieName("my-remember-me")
		            .rememberMeCookieDomain(rememberMeDomain)
		            .tokenValiditySeconds(63072000); // 2 years 60*60*24*365*2 = 63072000

			http.logout()
					.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
					.logoutSuccessUrl("/login");

			http.headers().frameOptions().disable();

		}
		
		@Override
	    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
	        auth.userDetailsService(userDetailsService);
	    }

		@Override
		@Bean
		public AuthenticationManager authenticationManagerBean() throws Exception {
			return super.authenticationManagerBean();
		}
	}
	
	@Bean
	PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
}
