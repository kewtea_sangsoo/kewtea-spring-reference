package com.example.demo.user.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.demo.mail.services.MailService;
import com.example.demo.user.models.ActivationLink;
import com.example.demo.user.models.User;
import com.example.demo.user.repositories.UserRepository;
import com.google.gson.Gson;

/**
 * (접두사로 Kew 가 붙은 이유는 중복을 없애기위해? 전에도 그래서 My 라는 접두사 붙였었음 ...)
 * @author sangsoolee
 */
@Service
public class KewUserService {

	@Autowired
	private MailService mailService;
	@Autowired
	private ActLinkService actLinkService;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Value("${spring.serverAddress}")
	private String serverAddress;
	
	Gson gson = new Gson();

	//

	public User createUser(User newUser) {
		try {
			long currentTime = System.currentTimeMillis();
			User user = userRepository.findByEmail(newUser.email);
			boolean isNewUser = user == null;
			if (isNewUser) {

				user = new User();
				user.createdtime = currentTime;
				user.email = newUser.email;
				user.lastmodifiedtime = currentTime;

				// encode password
				user.password = passwordEncoder.encode(newUser.password);

				// set name
				user.name = newUser.name;

				// set active status
				user.active = false;
				user = userRepository.save(user);
				
				// send activation email
				ActivationLink actLink = actLinkService.createActLink(user.id);
				HashMap<String, Object> msgs = new HashMap<>();
				msgs.put("userName", user.name);
				msgs.put("activateUrl", serverAddress+"/activateuser?code="+actLink.url);
				String body = mailService.buildBodyByTemplate("email/activation", msgs);
				mailService.send(user.email, "Naim Activation", body);

				return user;

			} else {
				return null;
			}
		} catch (Exception exception) {
			exception.printStackTrace();
			return null;
		}
	}
	
	public User getUser(long uid) {
		User user;
		
		try {
			user = userRepository.findById(uid);
		} catch(Exception e){
			return null;
		}
		
		return user;
	}
	
	public List<User> getUsersByIds(long[] ids) {
		List<User> response = new ArrayList<>();
		
		for (int i=0; i<ids.length; i++) {
			long id = ids[i];
			User user = userRepository.findById(id);
			if (null != user) response.add(user);
		}
		
		return response;
	}
	
	public List<User> findAllUsers() {
		return userRepository.findAll();
	}
	
	public User getUserByEmail(String email){
		return userRepository.findByEmail(email);
	}
	
	public User loginUser(String email, String password) {
		User user = getUserByEmail(email);
//		if (passwordEncoder.matches(password, user.password)) {
		if (password.equals(user.password)) {
			return user;
		} else {
			return null;
		}
	}
	
	// 질의하는 링크가 있으면 연결된 사용자를 active 상태로 바꾸어 준다 (이메일에서 링크를 누른 경우)
	public User activateUser(String actLinkUrl) throws Exception {
		try {
			
			// activate
			ActivationLink actLink = actLinkService.activateActLink(actLinkUrl);
			User user = userRepository.findById(actLink.userid);
			user.active = true;
			user = userRepository.save(user);

			// send welcome email
			HashMap<String, Object> msgs = new HashMap<>();
			msgs.put("userName", user.name);
			String body = mailService.buildBodyByTemplate("email/welcome", msgs);
			mailService.send(user.email, "Welcome to Kewtea "+user.name+"!", body);

			return user;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
