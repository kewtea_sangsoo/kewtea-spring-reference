package com.example.demo.user.services;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.user.models.ActivationLink;
import com.example.demo.user.repositories.ActLinkRepository;

@Service
public class ActLinkService {
	private static final long READY = -1L;
	private static final long EXPIRED = 0L;
	private static final long ACTIVATED = 1L;
	private static final long EXPIRE_TIME = 1000 * 60 * 30; // 30 minutes
	
	@Autowired
	private ActLinkRepository actLinkRepo;
	
	//
	
	// userId 를 통해 새로운 actLink 생성
	public ActivationLink createActLink(long userId) {

		// 이전에 발행하고 사용 안된것들 expire 시키기
		List<ActivationLink> oldActLinks = actLinkRepo.findByUserid(userId);
		for (ActivationLink oldActLink : oldActLinks) {
			if (oldActLink.status == READY) {
				oldActLink.status = EXPIRED;
				actLinkRepo.save(oldActLink);
			}
		}

		String actUrl = UUID.randomUUID().toString().replaceAll("-", "");
		long currentTime = System.currentTimeMillis();
		ActivationLink actLink = new ActivationLink(userId, actUrl, currentTime);
		return actLinkRepo.save(actLink);
	}

	// url 을 통해 activate
	public ActivationLink activateActLink(String url) throws Exception {
		ActivationLink actLink = actLinkRepo.findByUrl(url);
		if (actLink != null) {

			// check isExpired (time)
			long currentTime = System.currentTimeMillis();
			if (currentTime > actLink.createdtime+EXPIRE_TIME) {
				throw new Exception("Your activation link is expired.");
			}
			// check isExpired
			else if (actLink.status == READY) {
				actLink.status = ACTIVATED;
				return actLinkRepo.save(actLink);
			}
			else if (actLink.status == EXPIRED) {
				throw new Exception("Your activation link is expired. (requested re-activate)");
			}
			else if (actLink.status == ACTIVATED) {
				throw new Exception("You are already activated.");
			} else {
				throw new Exception("Unknown status: " + Long.toString(actLink.status));
			}

		} else {
			throw new Exception("Your activation link is not found. (non-exist url)");
		}
	}
	
}
