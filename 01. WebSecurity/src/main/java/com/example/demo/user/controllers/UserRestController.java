package com.example.demo.user.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.user.models.User;
import com.example.demo.user.services.KewUserService;

@RestController
@RequestMapping("/api")
public class UserRestController {
	
	@Autowired
	private KewUserService userService;
	
	//
	
	@RequestMapping(value="/createuser", method=RequestMethod.GET)
	long createUserByGet(
			@RequestParam(value="useremail") String useremail,
			@RequestParam(value="password") String pw,
			@RequestParam(value="name") String name) {
		
		User oldUser = userService.getUserByEmail(useremail);
		if (oldUser == null) {
			oldUser = new User();
			oldUser.email = useremail;
			oldUser.name = name;
			oldUser.password = pw;
			userService.createUser(oldUser);
			return 1L;
		} else {
			return -1L;
		}

	}
	
	@RequestMapping(value="/loginuser")
	User loginUser(@RequestParam(value="useremail") String useremail, @RequestParam(value="password") String password) {
		return userService.loginUser(useremail, password);
	}

}
