package com.example.demo.user.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.demo.user.MyUserDetails;
import com.example.demo.user.models.User;
import com.example.demo.user.repositories.UserRepository;

@Service
public class KewUserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {
	
	@Autowired
	UserRepository userRepository;
	
	@Override
	public UserDetails loadUserByUsername(String useremail) throws UsernameNotFoundException {
		User user = userRepository.findByEmail(useremail);
		if (null == user) throw new UsernameNotFoundException(String.format("User %s does not exist!", useremail));
		
		// *단어앞에 "ROLE_" 을 붙여야 .hasRole() 메소드에 걸린다
		// http://forum.spring.io/forum/spring-projects/security/51066-how-to-change-role-from-interceptor-url
		String authority = "ROLE_USER";
		if (user.email.contains("@kewtea.com")) authority = "ROLE_SUPERADMIN";
		java.util.Collection<? extends GrantedAuthority> authorities = AuthorityUtils.createAuthorityList(authority);
		
		return new MyUserDetails(user, authorities);
	}

}