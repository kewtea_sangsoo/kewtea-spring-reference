package com.example.demo.user.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Data
@Table(name = "users")
public class User implements Serializable {

	private static final long serialVersionUID = 1L;
	public User() {}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long id;

	@Email
	@Column(unique=true)
	public String email;

	@JsonIgnore  // Rest에서 패스워드 노출되는 것을 막을려면, 이렇게 한다 .., EncodedPassword면 60char이상 값이 저장될것임
	@Size(min = 4)
	@Column(nullable = true)
	public String password;

	@Column
	public String name;  // John Smith

	@Column(nullable = true)
	public long createdtime;

	@Column(nullable = true)
	public long lastmodifiedtime;

	@Column(columnDefinition="tinyint(1) default 0", nullable = true)
	public boolean active;

}
