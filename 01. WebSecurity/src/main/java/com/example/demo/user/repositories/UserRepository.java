package com.example.demo.user.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.user.models.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	// by id
	User findById(long id);
	
	// by email
	User findByEmail(String email);

}
