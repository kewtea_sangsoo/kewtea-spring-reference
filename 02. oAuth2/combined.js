function PlayerController($rootScope, $scope, dataService) {
    window.scope = $scope;

    /**
     * 0) scope variables
     */
    $scope.currentChannel = initialData.currentChannel;
    $scope.currentSceneId;

    $scope.slideList = {
        'scene-slide-0': {pos: 0},
        'scene-slide-1': {pos: 1},
        'scene-slide-2': {pos: 2},
    };
    $scope.slidePosList = [
        'scene-slide-p-0',
        'scene-slide-p-1',
        'scene-slide-p-2',
    ];

    $scope.shiftTimer = undefined;


    /**
     * 1) inner functions
     */

    function getDivIdByPosNum(posNum, scopeObject) {
        var foundId;
        var divIds = Object.getOwnPropertyNames(scopeObject);
        for (var i=0; i<divIds.length; i++) {
            var divId = divIds[i];
            if (scopeObject[divId].pos == posNum) foundId = divId;
        }
        return foundId;
    }

    function slideShift(direction) {
        var oldDivId = getDivIdByPosNum(1, $scope.slideList);
        var newDivId = getDivIdByPosNum(2, $scope.slideList);
        var oldSceneView = $scope.slideList[oldDivId].data;
        var newSceneView = $scope.slideList[newDivId].data;
        var hasNext = newSceneView !== undefined;

        if (hasNext) {
            var newSceneId = newSceneView.originData.id;

            // update position
            $scope.$applyAsync(function() {
                $scope.currentSceneId = newSceneId;
                for (var i=0; i<3; i++) {
                    var slideItem = $scope.slideList['scene-slide-'+i];
                    var posNum = slideItem.pos;
                    var newPosNum = shiftNumber(posNum, direction, 1, 2);
                    $scope.slideList['scene-slide-'+i].pos = newPosNum;
                }

                // oldSceneView 참조하여 지나간것 삭제
                oldSceneView.drawController.remove();

                // 새거 재생
                newSceneView.playView();

                // check buffer 로 변경?
                var nextSceneId =
                getNextSceneIdInChannelById($scope.currentChannel, newSceneId);
                dataService.getScenesByIds([nextSceneId])
                .then(function(scenes) {
                    var targetPosNum = (direction=='next') ? 2 : 0;
                    var targetDivId =
                        getDivIdByPosNum(targetPosNum, $scope.slideList);
                    var scene = scenes[0];
                    injectSlide(targetDivId, scene);
                });
            });
        } else {
            console.warn('next scene un-loaded');
        }
    }

    function shiftNumber(number, direction, offset, maxNum) {
        if (direction == 'prev') {
            number += offset;
            if (number > maxNum) number = number-(maxNum+1);
        } else {
            number -= offset;
            if (number < 0) number = (maxNum+number)+1;
        }
        return number;
    }

    function injectSlide(divId, sceneData) {
        return new Promise(function(resolve, reject) {
            if (sceneData) {
                buildSceneView(sceneData, divId).then(function(sceneView) {
                    $scope.$applyAsync(function() {
                        var currentDivId = getDivIdByPosNum(1, $scope.slideList);

                        sceneView.onPlayEnded = function() {
                            slideShift('next');
                        };

                        $scope.slideList[divId].data = sceneView;
                        if (currentDivId == divId) sceneView.playView();

                        resolve(sceneView);
                    });
                }, function(reason) {
                    console.error("buildSceneView failed", reason);
                });
            } else {
                $scope.$applyAsync(function() {
                    var sceneView = $scope.slideList[divId].data;
                    if (sceneView) {
                        sceneView.remove();
                        $scope.slideList[divId].data = undefined;
                    }
                    resolve();
                });
            }
        });
    }

    function buildSceneView(scene, targetDivId) {
        var themeId = scene.themeid;
        return new Promise(function(resolve, reject) {

            dataService.getThemesByIds([themeId])
            .then(function(themes) {
                var theme = themes[0];
                if (theme) {
                    build(theme, scene);
                } else {
                    console.warn("theme not available. wait for sync...");
                    $rootScope.$on('onSetTheme', function(event, theme) {
                        if (theme.id == themeId) {
                            build(theme, scene);
                        }
                    });
                    // timeout
                    setTimeout(function() {
                        // check if still waiting
                        // -> pass current slide, .. shiftSlide('next');
                    }, 1000*15);
                }
            });

            function build(theme, scene) {
                var dataset = JSON.parse(JSON.stringify(scene.data));
                delete dataset.periodlength;
                console.warn(dataset);
                try {
                    LA.init({
                        selector: '#'+targetDivId,
                        size: {width: '100%', height: '100%'},
                        dataset: dataset,
                        options: {
                            script: {
                                url: theme.scriptformat.url,
                                controller: theme.scriptformat.controller,
                            },
                        },
                    }).then(function(controller) {
                        var sceneView = new SceneView();
                        sceneView = sceneView.buildView(scene, theme, controller, targetDivId);
                        resolve(sceneView);
                    }, function(reason) {
                        reject(reason);
                    });
                } catch (error) {
                    console.error(error);
                    reject(error);
                }
            }
        });
    }

    /**
     * 2) listener functions
     */

    function init(initType, args) {

        switch (initType) {

            case 'scenePages' :
                initScenePages(args.currentPageId);
            break;

            case 'resize' :
                initResize();
            break;

            default:
                initResize();
                initScenePages();
        }

        function initScenePages(currentPageId) {
            if (currentPageId == undefined)
                currentPageId = $scope.currentSceneId;

            var sceneIds = getSceneIdsInChannel($scope.currentChannel);
            var nextSceneId =
            getNextSceneIdInChannelById($scope.currentChannel, currentPageId);

            dataService.getScenesByIds(sceneIds)
            .then(function(scenes) {
                for (var i=0; i<scenes.length; i++) {
                    var scene = scenes[i];
                    if (currentPageId == scene.id) {
                        var currentDivId =
                            getDivIdByPosNum(1, $scope.slideList);
                        injectSlide(currentDivId, scene);
                    }
                    if (nextSceneId == scene.id) {
                        var nextDivId = getDivIdByPosNum(2, $scope.slideList);
                        injectSlide(nextDivId, scene);
                    }
                }
            });
        }

        function initResize() {
            var wW = $(window).width();
            var newCss = '<style id="slidePosCss">\n';

            // day slide css
            newCss += '.scene-slide-p-0 { left: -'+wW+'px }\n'+
                '.scene-slide-p-1 { left: 0 }\n'+
                '.scene-slide-p-2 { left: '+wW+'px }\n';
            newCss += '</style>';

            $('head').find('#slidePosCss').remove();
            $('head').append(newCss);

            // 원규형쪽에서 resize function 만들어지면 적용하기
            var currentDivId = getDivIdByPosNum(1, $scope.slideList);
            var currentSceneView = $scope.slideList[currentDivId].data;
            if (currentSceneView) {
                var scene = currentSceneView.originData;
                var theme = currentSceneView.themeData;
                currentSceneView.drawController.remove();
                var dataset = JSON.parse(JSON.stringify(scene.data));
                delete dataset.periodlength;
                console.warn(dataset);
                LA.init({
                    selector: '#'+currentDivId,
                    size: {width: '100%', height: '100%'},
                    dataset: dataset,
                    options: {
                        script: {
                            url: theme.scriptformat.url,
                            controller: theme.scriptformat.controller,
                        },
                    },
                }).then(function(controller) {
                    var sceneView = new SceneView();
                    sceneView = sceneView.buildView(scene, theme, controller, currentDivId);
                    sceneView.onPlayView = currentSceneView.onPlayView;
                    sceneView.onPlayEnded = currentSceneView.onPlayEnded;
                    $scope.slideList[currentDivId].data = sceneView;
                    sceneView.drawController.draw();
                });
            }
        }
    }

    var rtime;
    var timeout = false;
    var resizeDuration = 100;
    function onResize() {
        rtime = new Date();
        if (timeout === false) {
            timeout = true;
            setTimeout(resizeEnd, resizeDuration);
        }
    }

    function resizeEnd() {
        if (new Date() - rtime < resizeDuration) {
            setTimeout(resizeEnd, resizeDuration);
        } else {
            timeout = false;
            init('resize');
        }
    }

    function getSceneIdsInChannel(channel) {
        var ids = [];
        var timeTable = channel['timetable'];
        var keys = Object.getOwnPropertyNames(timeTable);
        for (var i=0; i<keys.length; i++) {
            var key = keys[i];
            var id = timeTable[key];
            ids.push(id);
        }
        return ids;
    }

    function getNextSceneIdInChannelById(channel, id) {
        var timeTable = channel['timetable'];
        var keys = Object.getOwnPropertyNames(timeTable);

        // parse to number
        for (var i=0; i<keys.length; i++) {
            keys[i] = JSON.parse(keys[i]);
        }
        keys.sort(function (a,b) { return a-b; });
        var ids = [];
        for (var i=0; i<keys.length; i++) {
            ids.push(timeTable[keys[i]]);
        }
        var targetKey = keys[ids.indexOf(id)+1];
        if (targetKey) {
            return timeTable[targetKey];
        } else {
            return timeTable[keys[0]];
        }
    }

    /**
     * 3) Broadcast Receivers ....
     */

    $rootScope.$on('onSetScene', function(event, scene) {
        console.log('onSetScene', scene);

        var nextSceneId = getNextSceneIdInChannelById(
            $scope.currentChannel,
            $scope.currentSceneId
        );
        var isNextScene = nextSceneId == scene.id;
        var isCurrentScene = $scope.currentSceneId == scene.id;

        console.warn("nextSceneId="+nextSceneId);
        console.warn("currentSceneId="+$scope.currentSceneId);


        var indexList = [];
        for (var i=0; i<3; i++) {
            var sceneeView = $scope.slideList['scene-slide-'+i].data;
            // 이미 있던 data 이면서, 변경요청일 경우
            if (sceneeView && sceneeView.id == scene.id) {
                var oldData = sceneeView.originData;
                if (oldData.lastmodifiedtime < scene.lastmodifiedtime) {
                    indexList.push(i);
                }
            }
        }

        if (indexList.length == 0) {
            for (var i=0; i<3; i++) {
                if (isCurrentScene && $scope.slideList['scene-slide-'+i].pos == 1) {
                    indexList.push(i);
                }
                if (isNextScene && $scope.slideList['scene-slide-'+i].pos == 2) {
                    indexList.push(i);
                }
            }
        }

        indexList.forEach(function(sceneIndex) {
            var divId = 'scene-slide-'+sceneIndex;
            buildSceneView(scene, divId).then(function(sceneView) {
                sceneView.onPlayEnded = function() {
                    slideShift('next');
                };
                $scope.slideList[divId].data = sceneView;
                if (isCurrentScene) sceneView.playView();
            }, function(reason) {
                console.error("buildSceneView failed", reason);
            });
        });

    });

    $rootScope.$on('onSetTheme', function(event, theme) {
        console.log('onSetTheme', theme);
        // 1) theme 이 없어서 못그린 것들 그리기
        // ...
    });

    function getCurrentSceneId() {
        if ($scope.currentSceneId) {
            return $scope.currentSceneId;
        } else {
            var channel = $scope.currentChannel;
            var timeTable = channel['timetable'];
            var keys = Object.getOwnPropertyNames(timeTable);

            // parse to number
            for (var i=0; i<keys.length; i++) {
                keys[i] = JSON.parse(keys[i]);
            }

            // if relative timetable
            keys.sort(function (a,b) { return a-b; });
            return timeTable[keys[0]];

            // if absolute timetable
            // get current time .. 계산하여 현재 시간값에 해당하는 scene 찾아서 play
            // ...
        }
    }

    $scope.onLoadController = function() {
        $scope.currentSceneId = getCurrentSceneId();
        init();
    };

    $(window).resize(onResize);

    window.slideShift = slideShift;
}

function DataService($rootScope, $http, serviceConfig, initialData) {
    var adapter;

    var sceneKey = serviceConfig.localKeyRoot+'scene'+serviceConfig.localKeyDivider;
    var themeKey = serviceConfig.localKeyRoot+'theme'+serviceConfig.localKeyDivider;

    this.initAdapter = function(adapterType, serviceType) {
        console.log(adapterType, serviceType);
        /** set adapter */

        if ('SessionStorageHttpAdapter' == adapterType) {
            if ('web' == serviceType) {
                CacheManager.prototype.onSetEvent =
                    function(key, value) {
                        console.log('SessionStorageHttpAdapter:onSetEvent', key);
                        if (-1 < key.indexOf(sceneKey))
                            $rootScope.$broadcast('onSetScene', value);
                        if (-1 < key.indexOf(themeKey))
                            $rootScope.$broadcast('onSetTheme', value);
                    };

                var cacheManager = new CacheManager(serviceConfig.cacheStorage, serviceConfig.cacheSize);

                adapter = new SessionStorageHttpAdapter(
                    serviceConfig.localKeyRoot,
                    serviceConfig.localKeyDivider,
                    cacheManager,
                    serviceConfig.hostUrl,
                    serviceConfig.baseUrl,
                    initialData,
                    $rootScope,
                    $http,
                    serviceConfig.useOauth,
                    serviceConfig.syncIntervalSec
                );

            }
        }

        /** set methods */

        this.getScenesByIds = adapter.getScenesByIds;
        this.getThemesByIds = adapter.getThemesByIds;
        // ...

    };

    this.initAdapter(serviceConfig.initAdapter, serviceConfig.serviceType);
}