
요약
---
- **Spring Boot 1~ 버전대로 사용해야함 (2 버전대는 oAuth2 가 제대로 지원되고있지 않음)**
- MySQL
- 기본 회원가입 기능 (가입, 인증, 로그인) + WebSecurity + oAuth2
- Mailjet 을 통한 메일링 서비스도 구현 되어있음


/signup 에서 회원 가입 가능 (이메일 발송까지 확인 가능)
해당 로그인 정보로 oAuth2 토큰 발행 가능 (postman 등 테스트앱을 활용)<br>
ex) 

```
Method: POST
URL: localhost:8080/oauth/token?grant_type=password&username=sangsoo.lee@kewtea.com&password=1234
Header: =Authorization: Basic bXktdHJ1c3RlZC1jbGllbnQ6c2VjcmV0 

Response:
{
    "access_token": "13255efa-719b-4a39-b7b8-3f843f83ad75",
    "token_type": "bearer",
    "refresh_token": "05384f3a-70b2-4bfb-8684-658b13750093",
    "expires_in": 31537292,
    "scope": "read write"
}

```

Header 에 Authorization 는 js에서는 아래와 같이 써야함
`'Authorization': 'Basic '+btoa('my-trusted-client:secret')`

* * *

mysql 기본세팅 (application.properties 에서 수정가능)
--- 
- database: demo
- user: root / 1234

