//jasmine을 통한 테스트 방법 jasmine tech.review/spec/integration-test.js

var serverUrl = "http://localhost:8080";
var selenium = require('selenium-webdriver');
var driver;

describe('Selenium Tutorial', function() {

    function waitForComment() {
        driver.wait(
            selenium.until.elementLocated(selenium.By.className('dayPage-comments')),
            10000,
            'Could not locate the child element within the time specified'
        );
    }

    function userSetup(userType) {

        beforeEach(function(done) {

            driver = new selenium.Builder()
                .withCapabilities(selenium.Capabilities.chrome())
                .build();

            if (userType == "anonymous") {
                driver.get(serverUrl+'/journal/@sangsoo/20170406').then(function() {
                    waitForComment();
                    done();
                });
            } else {
                driver.get(serverUrl+"/login").then(function() {

                    if (userType == "owner") {
                        driver.findElement(selenium.By.name('username')).sendKeys("sangsoo.lee@kewtea.com");
                        driver.findElement(selenium.By.name('password')).sendKeys("1234");
                    } else {
                        driver.findElement(selenium.By.name('username')).sendKeys("lss6436@gmail.com");
                        driver.findElement(selenium.By.name('password')).sendKeys("1234");
                    }

                    driver.findElement(selenium.By.name('password')).sendKeys(selenium.Key.ENTER);
                    waitForComment();

                    driver.get(serverUrl+'/journal/@sangsoo/20170406').then(function() {
                        waitForComment();
                        done();
                    });

                });
            }

        });

        afterEach(function(done) {
            if (userType == "anonymous") {
                driver.quit().then(done);
            } else {
                driver.get(serverUrl+"/logout/").then(function() {
                    driver.quit().then(done);
                });
            }
        });

    }

    function getCommentNumber() {
        return new Promise(function(resolve, reject) {
            driver.findElement(selenium.By.id("day-slide-1"))
            .then(function(currentDatePage) {
                currentDatePage.findElements(selenium.By.className("day-comment-wrapper"))
                .then(function(commentViews) {
                    resolve(commentViews.length);
                });
            });
        });
    }


    // 로그인 안한 익명일 경우, 보이는 커맨트 갯수 (1개인지)
    describe("Anonymous User", function() {
        userSetup("anonymous");
        it('1 comment visible', function(done) {
            getCommentNumber().then(function(number) {
                expect(number).toBe(1);
                done();
            });
        });
    });

    // 주인이 들어왔을 경우 (2개인지)
    describe("Owner User", function() {
        userSetup("owner");
        it('2 comment visible', function(done) {
            getCommentNumber().then(function(number) {
                expect(number).toBe(2);
                done();
            });
        });
    });

    // 주인이 아닌 유저 (1개인지)
    describe("Logged in User", function () {
        userSetup("other");
        it('1 comment visible', function(done) {
            getCommentNumber().then(function(number) {
                expect(number).toBe(1);
                done();
            })
        });
    });

});