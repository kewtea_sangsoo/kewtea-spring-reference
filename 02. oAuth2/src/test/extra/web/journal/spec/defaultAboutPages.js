//jasmine을 통한 테스트 방법 jasmine tech.review/spec/integration-test.js
jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;
// TODO -> 'http://localhost:8080', 'https://journal.kewtea.com'
var serverUrl = "http://localhost:8080";
var selenium = require('selenium-webdriver');
var driver;
var user = {
    "username": "sangsoo.lee@kewtea.com",
    "password": "1234"
};
describe('Selenium Tutorial', function() {

    function setup(userObject, path) {

        beforeEach(function(done) {

            driver = new selenium.Builder()
                .withCapabilities(selenium.Capabilities.chrome())
                .build();

            if (userObject == null) {
                driver.get(serverUrl+path).then(done);
            } else {
                driver.get(serverUrl+"/login").then(function() {
                    driver.findElement(selenium.By.name('username')).sendKeys(userObject["username"]);
                    driver.findElement(selenium.By.name('password')).sendKeys(userObject["password"]);
                    driver.findElement(selenium.By.name('password')).sendKeys(selenium.Key.ENTER);
                    driver.get(serverUrl+path).then(done);
                });
            }

        });

        afterEach(function(done) {
            if (userObject == null) {
                driver.quit().then(done);
            } else {
                driver.get(serverUrl+"/logout/").then(function() {
                    driver.quit().then(done);
                });
            }
        });

    }

    // 특이하고 영속적인 DOM element 하나를 get 해보고 있으면 pass 없으면 fail
    function getUniqueElement(elementId) {
        return new Promise(function(resolve, reject) {
            resolve(driver.findElement(selenium.By.id(elementId)));
        });
    }

    /**
     * anonymous, logged-in user (sangsoo.lee@kewtea.com)
     * /kewtea
     * /kewtea/about
     * /journal
     * /journal/@sangsoo
     */
    describe("anonymous user, /kewtea", function() {

        setup(null, "/kewtea");
        it("check DOM", function(done) {
            getUniqueElement("custom-search-input")
            .then(function(element) {
                expect(element).not.toBe(null);
                done();
            }, function(reason) {
                done();
            });
        });

    });

    describe("anonymous user, /kewtea/about", function() {

        setup(null, "/kewtea/about");
        it("check DOM", function(done) {
            getUniqueElement("page-top")
            .then(function(element) {
                expect(element).not.toBe(null);
                done();
            }, function(reason) {
                done();
            });
        });

    });

    describe("anonymous user, /journal", function() {

        setup(null, "/journal");
        it("check DOM", function(done) {
            getUniqueElement("slide-down")
            .then(function(element) {
                expect(element).not.toBe(null);
                done();
            }, function(reason) {
                done();
            });
        });

    });

    describe("anonymous user, /journal/@sangsoo", function() {

        setup(null, "/journal/@sangsoo");
        it("check DOM", function(done) {
            getUniqueElement("first-glass")
            .then(function(element) {
                expect(element).not.toBe(null);
                done();
            }, function(reason) {
                done();
            });
        });

    });

    describe("logged-in user, /kewtea", function() {

        setup(user, "/kewtea");
        it("check DOM", function(done) {
            getUniqueElement("custom-search-input")
            .then(function(element) {
                expect(element).not.toBe(null);
                done();
            }, function(reason) {
                done();
            });
        });

    });

    describe("logged-in user, /kewtea/about", function() {

        setup(user, "/kewtea/about");
        it("check DOM", function(done) {
            getUniqueElement("page-top")
            .then(function(element) {
                expect(element).not.toBe(null);
                done();
            }, function(reason) {
                done();
            });
        });

    });

    describe("logged-in user, /journal", function() {

        setup(user, "/journal");
        it("check DOM", function(done) {
            getUniqueElement("slide-down")
            .then(function(element) {
                expect(element).not.toBe(null);
                done();
            }, function(reason) {
                done();
            });
        });

    });

    describe("logged-in user, /journal/@sangsoo", function() {

        setup(user, "/journal/@sangsoo");
        it("check DOM", function(done) {
            getUniqueElement("first-glass")
            .then(function(element) {
                expect(element).not.toBe(null);
                done();
            }, function(reason) {
                done();
            });
        });

    });

});