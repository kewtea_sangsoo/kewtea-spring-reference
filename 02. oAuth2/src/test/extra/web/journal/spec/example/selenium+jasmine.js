//jasmine을 통한 테스트 방법 jasmine tech.review/spec/integration-test.js

var selenium = require('selenium-webdriver');
var driver;

describe('Selenium Tutorial', function() {

    // Open the TECH.insight website in the browser before each test is run
    beforeEach(function(done) {

        driver = new selenium.Builder()
            .withCapabilities(selenium.Capabilities.chrome())
            .build();
        driver.get('http://www.google.com').then(function() {
            var qInput = driver.findElement(selenium.By.name('q'));
                qInput.sendKeys('webdriver');
                qInput.sendKeys(selenium.Key.ENTER);
            driver.sleep(500).then(done);
        });
    });

    // Close the website after each test is run (so that it is opened fresh each time)
    afterEach(function(done) {
        driver.quit().then(done);
    });

    // Test to ensure we are on the home page by checking the <body> tag id attribute
    it('Should be on the home page', function(done) {
        driver.getTitle().then(function(title) {
            console.log(title);
            expect(title.indexOf('webdriver -')).toBeGreaterThan(-1);
            done();
        });
    });

});