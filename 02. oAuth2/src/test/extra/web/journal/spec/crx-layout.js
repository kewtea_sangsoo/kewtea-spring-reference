//jasmine을 통한 테스트 방법 jasmine tech.review/spec/integration-test.js
jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

var selenium = require('selenium-webdriver');
var chrome = require('selenium-webdriver/chrome');
// chrome 시작 시, 테스트 하고싶은 extension load
var options = new chrome.Options().addArguments("load-extension=/Users/sangsoolee/Documents/workspace/journal-extension");

var driver;

describe('CRX Layout Test By Selenium', function() {

    var sideNoteHeaderInfo = {};
    var sideNoteBodyInfo = {};
    var scrapBookHeaderInfo = {};
    var scrapBookBodyInfo = {};

    function setup() {

        beforeEach(function(done) {

            driver = new selenium.Builder()
                .forBrowser('chrome')
                .setChromeOptions(options)
                .build();
            driver.manage().window().setSize(1400, 1600);

            var siteUrl = "https://kewtea.com/about";
            driver.get("chrome-extension://dgoijijiiclolchhdioigdmdjiipdmgj/background.html").then(function() {
                driver.sleep(1000).then(function() {
                    // test 용 js 명령어 보내보기 ...
                    driver.executeScript(
                        'chrome.tabs.create({url: "'+siteUrl+'"}, function(tab){'+
                            'showSidebar(tab)'+
                        '});'
                    ).then(function() {
                        driver.sleep(1000*5).then(function() {

                            // switch to new tab
                            driver.getAllWindowHandles().then(function(tabs) {
                                driver.switchTo().window(tabs[1]);

                                driver.findElement(selenium.By.id("kt-annotator-panel")).then(function(iframe) {
                                    driver.switchTo().frame(iframe);
                                    driver.findElement(selenium.By.id("panel-ctrl-btn")).click();
                                    driver.sleep(500).then(done);

                                    /*
                                    driver.findElement(selenium.By.id("panel-ctrl-btn")).click();
                                    driver.sleep(500).then(function() {
                                        driver.findElement(selenium.By.id("setting-head")).click();
                                        driver.sleep(500).then(function() {
                                            driver.findElement(selenium.By.id("account-head")).click();
                                            driver.sleep(500).then(function() {
                                                driver.findElement(selenium.By.name("email")).sendKeys(useremail);
                                                driver.findElement(selenium.By.name("password")).sendKeys(password);
                                                driver.findElement(selenium.By.id("login-submit")).click();
                                                driver.sleep(1000*5).then(function() {
                                                    driver.findElement(selenium.By.id("side-comments-head")).click();
                                                    driver.sleep(500).then(done);
                                                });
                                            });
                                        });
                                    });
                                    */


                                });

                            });

                        });

                    });
                });

            });

        });

        afterEach(function(done) {
            driver.quit().then(done);
        });

    }

    function getSizeAndLocation(webElement) {
        return new Promise(function (resolve, reject) {
            var sizeAndLocation = {};
            webElement.getSize().then(function(size) {
                sizeAndLocation["size"] = size;
                webElement.getLocation().then(function(location) {
                    sizeAndLocation["location"] = location;
                    resolve(sizeAndLocation);
                });
            });
        });
    }


    describe("layout", function() {

        setup();

        /**
         * (width,height) / (x, y)
         * sideNote - section-header -> 300, 42 / 0, 0
         * sideNote - section-body  -> 300, 1339 / 0, 42
         * scrapBook - section-header -> 600, 42 / 300, 0
         * scrapBook - section-body -> 600, 1339 / 300, 42
         */
        it("check size and location", function(done) {
            var promiseList = [];

            var sideNote = driver.findElement(selenium.By.className("sidenote-section"));
            var scrapBook = driver.findElement(selenium.By.className("scrapbook-section"));

            var sideNoteHeader = sideNote.findElement(selenium.By.className("section-header"));
            var sideNoteBody = sideNote.findElement(selenium.By.className("section-body"));
            var scrapBookHeader = scrapBook.findElement(selenium.By.className("section-header"));
            var scrapBookBody = scrapBook.findElement(selenium.By.className("section-body"));

            promiseList.push(
                getSizeAndLocation(sideNoteHeader).then(function(sizeAndLocation) {
                    expect(sizeAndLocation["size"].width).toBe(300);
                    expect(sizeAndLocation["size"].height).toBe(42);
                    expect(sizeAndLocation["location"].x).toBe(0);
                    expect(sizeAndLocation["location"].y).toBe(0);
                })
            );

            promiseList.push(
                getSizeAndLocation(sideNoteBody).then(function(sizeAndLocation) {
                    expect(sizeAndLocation["size"].width).toBe(300);
                    expect(sizeAndLocation["size"].height).toBe(1339);
                    expect(sizeAndLocation["location"].x).toBe(0);
                    expect(sizeAndLocation["location"].y).toBe(42);
                })
            );

            promiseList.push(
                getSizeAndLocation(scrapBookHeader).then(function(sizeAndLocation) {
                    expect(sizeAndLocation["size"].width).toBe(600);
                    expect(sizeAndLocation["size"].height).toBe(42);
                    expect(sizeAndLocation["location"].x).toBe(300);
                    expect(sizeAndLocation["location"].y).toBe(0);
                })
            );

            promiseList.push(
                getSizeAndLocation(scrapBookBody).then(function(sizeAndLocation) {
                    expect(sizeAndLocation["size"].width).toBe(600);
                    expect(sizeAndLocation["size"].height).toBe(1339);
                    expect(sizeAndLocation["location"].x).toBe(300);
                    expect(sizeAndLocation["location"].y).toBe(42);
                })
            );

            Promise.all(promiseList).then(done);

        });

    });

});