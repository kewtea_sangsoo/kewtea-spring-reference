/**
 * Created by sangsoolee on 2017. 3. 2..
 */
jasmine.DEFAULT_TIMEOUT_INTERVAL = 60000;

// require (import)
var UtilService = require("./MockUtilService.js");
var CacheManager = require("./MockCacheManager");
var HttpService = require("./MockHttpService");
var DataParser = require("./MockDataParser");
var SyncAdapter = require("../../../../../src/main/resources/static/journal/app/SyncAdapter2");

// cacheManager, httpService, utilService, 등을 테스트용으로 생성 (무조건 맞는값)
// httpService -> httpBackend 는 random 하게 오답/실패를 return 함

// syncAdapter 가 성공 시 -> cacheManager 에 set
// syncAdapter 가 실패 시 -> syncList 에 다시 추가후, 재시도 -> cacheManager 에 set
describe("TestSyncAdapter2", function() {

    var cleanToken = {
        "access_token": "abc1234",
        "refresh_token": "abcd12345"
    };
    var dirtyToken = {
        "access_token": "1234abc",
        "refresh_token": "abcd12345"
    };

    var storage = {};

    CacheManager.prototype.onSetEvent = function(key, value) {
        storage[key] = value;
    };

    var utilService = new UtilService();
    var cacheManager = new CacheManager();
    var httpService = new HttpService("office");
    var dataParser = new DataParser();
    var syncAdapter = new SyncAdapter(cacheManager, httpService, dataParser, utilService);

    describe("scenario #1, try server call (SyncAdapter 기본 동작확인)", function() {

        beforeEach(function(done) {
            storage = {};

            syncAdapter.addSyncList("getCommentsByUserIdAndInventoryId", [1, 1]);

            setTimeout(done, 1000);
        });

        it("check Synced", function() {
            var keys = Object.getOwnPropertyNames(storage);
            expect(keys.length).toBe(2);
        });

    });

    describe("scenario #2, try server call with clean token (기본동작+토큰처리)", function() {

        beforeEach(function(done) {
            storage = {};

            // .. token 발행 과정을 거쳐 access token 을 얻어냈다고 가정
            syncAdapter.setAuthToken(cleanToken);

            syncAdapter.addSyncList("getCommentsByUserIdAndInventoryId", [1, 2]);

            setTimeout(done, 1000);
        });

        it("check Synced", function() {
            var keys = Object.getOwnPropertyNames(storage);
            expect(keys.length).toBe(1);
        });

    });

    describe("scenario #3, try server call with expired token", function() {

        // 비정상 토근으로 1회 요청 후, 자동으로 재발행 기능 수행하여 데이터를 가져오는 지 test

        beforeEach(function(done) {
            storage = {};

            // 비정상 토큰 set
            syncAdapter.setAuthToken(dirtyToken);

            syncAdapter.addSyncList("getCommentsByUserIdAndInventoryId", [1, 1]);

            setTimeout(done, 5000);
        });

        it("check Synced", function() {
            var keys = Object.getOwnPropertyNames(storage);
            expect(keys.length).toBe(2);
        });

    });

    describe("scenario #4, try server call with expired token, extreme test", function() {

        // 만료된 토큰으로 짧은시간에, 여러번 addSyncList 를 해보고,
        // 자동으로 재 등록을 거쳐 최후에 데이터가 모두 정상적으로 오는지 등을 테스트

        beforeEach(function(done) {
            storage = {};

            // 비정상 토큰 set
            syncAdapter.setAuthToken(dirtyToken);

            // 동시다발적인 요청들
            var i = 0;
            while (i < 10) {

                if (i<3) {
                    syncAdapter.addSyncList("getCommentsByUserIdAndInventoryId", [1, 1]);
                } else {
                    if (i<7) {
                        syncAdapter.addSyncList("getCommentsByUserIdAndInventoryId", [1, 2]);
                    } else {
                        syncAdapter.addSyncList("getCommentsByUserIdAndInventoryId", [1, 3]);
                    }
                }

                i++;
            }

            syncAdapter.addSyncList("getCommentsByUserIdAndInventoryId", [1, 4]);

            setTimeout(done, 5000);

        });

        it("check Synced", function() {
            var keys = Object.getOwnPropertyNames(storage);
            expect(keys.length).toBe(5);
        });

    });


    /** 추후, 최적화 코드 추가 후, Test 용 */
    /*
    describe("scenario #5, try server call in bad network", function() {

        // getCallNum 등을 하여, 최적화 등이 잘 되어잇는지 점검 (같은 call 은 하나로 처리해서 보냈는지, 등)

    });
    */

    /*
    describe("scenario #6, try server call in bad network", function() {

    });
    */

});