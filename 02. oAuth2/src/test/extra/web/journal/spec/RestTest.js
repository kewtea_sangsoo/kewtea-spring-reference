// 'http://localhost:8080', 'https://journal.kewtea.com'
var SERVER_URL = 'https://journal.kewtea.com';

// set access token
// 직접 curl 로 token 값을 받아와서 여기에 적어놓고 test run 할것.
// curl -d "grant_type=password&username=sangsoo.lee@kewtea.com&password=1234" -H "Authorization: Basic bXktdHJ1c3RlZC1jbGllbnQ6c2VjcmV0" http://localhost:8080/oauth/token
var token = '5af09523-cb1e-445e-bfc7-1f3cf6536f16'; // access token 값을 여기에 적으시오

var $ = require('jquery');
XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest;

// TODO 이거 변형&확장 하여서, HttpService.setMethodsByJSON() ...
// 그 하나의 config json 으로 test 도 하고 실제 httpService 생성도 하자
var testURLs = {
    'getContentsByIds': {
        'methodType': 'GET',
        'requestUrl': SERVER_URL+'/api/journal/contents?ids=1,2,3',
    },
    'getNextDatePagesById': {
        'methodType': 'GET',
        'requestUrl': SERVER_URL+
        '/api/journal/datepage?id=20&direction=next&offset=3',
    },
    'getUrlPageByUserIdAndUrl': {
        'methodType': 'GET',
        'requestUrl': SERVER_URL+
        '/api/journal/urlpage?userid=1&url=http%3A%2F%2Fwww.bbc.com%2F',
    },
    'getCommentsByUserIdAndDateFromTo': {
        'methodType': 'GET',
        'requestUrl': SERVER_URL+
        '/api/journal/comments?userid=1&' +
        'datefrom=1487602800000&dateto=1487689199000',
    },
    'getCommentsByInventoryId': {
        'methodType': 'GET',
        'requestUrl': SERVER_URL+'/api/journal/comments?inventoryid=1',
    },
    'getUncategorizedComments': {
        'methodType': 'GET',
        'requestUrl': SERVER_URL+
        '/api/journal/comments?userid=1&inventoryid=-1',
    },
    'getCommentsByUrlPageId': {
        'methodType': 'GET',
        'requestUrl': SERVER_URL+'/api/journal/comments?urlpageid=5',
    },
    'getCommentsByUserIdAndUrl': {
        'methodType': 'GET',
        'requestUrl': SERVER_URL+
        '/api/journal/comments?userid=1&url=http%3A%2F%2Fwww.bbc.com%2F',
    },
    'getCommentsByIds': {
        'methodType': 'GET',
        'requestUrl': SERVER_URL+'/api/journal/comments?ids=1,2,3',
    },
    'getCollectionsByUserId': {
        'methodType': 'GET',
        'requestUrl': SERVER_URL+'/api/journal/collections?userid=1',
    },
    'getKeywordsByUserId': {
        'methodType': 'GET',
        'requestUrl': SERVER_URL+'/api/journal/keywords?userid=1',
    },
    'getInventoriesByIds': {
        'methodType': 'GET',
        'requestUrl': SERVER_URL+'/api/journal/inventories?ids=1,2,3',
    },
    'getDatePagesByUserIdAndYearMonth': {
        'methodType': 'GET',
        'requestUrl': SERVER_URL+
        '/api/journal/datepage?userid=1&year=2017&month=1',
    },
    'getUserByEmail': {
        'methodType': 'GET',
        'requestUrl': SERVER_URL+
        '/api/getuser/useremail=sangsoo.lee@kewtea.com',
    },
    'getSettingByUserId': {
        'methodType': 'GET',
        'requestUrl': SERVER_URL+'/api/journal/settings?userid=1',
    },
    'getStorageSizeByUserId': {
        'methodType': 'GET',
        'requestUrl': SERVER_URL+'/api/journal/getsize?userid=1',
    },
};


describe('REST API Test', function() {
    function BuildRequest(type, url, body, callback) {
        var timeOffset = -new Date().getTimezoneOffset().toString();
        var request = new XMLHttpRequest();
            request.open(type, url, true);
            request.setRequestHeader('Content-Type', 'application/json');
            request.setRequestHeader('Authorization', 'Bearer '+token);
            request.setRequestHeader('TimeOffset', timeOffset);
            request.onreadystatechange = function() {
                if (request.readyState == 4) {
                    var response = null;
                    callback(request.status);
                }
            };
        return request;
    }

    for (var key in testURLs) {
        if (Object.prototype.hasOwnProperty.call(testURLs, key) ||
            {}.hasOwnProperty.call(testURLs, key)) {
            var testURL = testURLs[key];
            describe(testURL, function() {
                var responseStatus = -1;
                beforeEach(function(done) {
                    var request = new BuildRequest(
                        testURL['methodType'],
                        testURL['requestUrl'],
                        testURL['requestBody'],
                        function(status) {
                            responseStatus = status;
                            done();
                        }
                    );
                    request.send(null);
                });
                it('response is OK', function() {
                    expect(responseStatus).toBe(200);
                });
            });
        }
    }
});
