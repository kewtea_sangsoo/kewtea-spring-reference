//jasmine을 통한 테스트 방법 jasmine tech.review/spec/integration-test.js
jasmine.DEFAULT_TIMEOUT_INTERVAL = 60000;

var selenium = require('selenium-webdriver');
var chrome = require('selenium-webdriver/chrome');
// chrome 시작 시, 테스트 하고싶은 extension load
var options = new chrome.Options().addArguments("load-extension=/Users/sangsoolee/Documents/workspace/journal-extension");

var driver;

describe('Selenium Tutorial', function() {

    function setup(useremail, password) {

        beforeEach(function(done) {

            driver = new selenium.Builder()
                .forBrowser('chrome')
                .setChromeOptions(options)
                .build();

            var siteUrl = "https://kewtea.com/about";
            driver.get("chrome-extension://dgoijijiiclolchhdioigdmdjiipdmgj/background.html").then(function() {
                driver.sleep(1000).then(function() {
                    // test 용 js 명령어 보내보기 ...
                    driver.executeScript(
                        'chrome.tabs.create({url: "'+siteUrl+'"}, function(tab){'+
                            'showSidebar(tab)'+
                        '});'
                    ).then(function() {
                        driver.sleep(1000*5).then(function() {

                            // switch to new tab
                            driver.getAllWindowHandles().then(function(tabs) {
                                driver.switchTo().window(tabs[1]);

                                driver.findElement(selenium.By.id("kt-annotator-panel")).then(function(iframe) {
                                    driver.switchTo().frame(iframe);
                                    driver.findElement(selenium.By.id("panel-ctrl-btn")).click();
                                    driver.sleep(500).then(function() {
                                        driver.findElement(selenium.By.id("setting-head")).click();
                                        driver.sleep(500).then(function() {
                                            driver.findElement(selenium.By.id("account-head")).click();
                                            driver.sleep(500).then(function() {
                                                driver.findElement(selenium.By.name("email")).sendKeys(useremail);
                                                driver.findElement(selenium.By.name("password")).sendKeys(password);
                                                driver.findElement(selenium.By.id("login-submit")).click();
                                                driver.sleep(1000*10).then(done);
                                            });
                                        });
                                    });


                                });

                            });

                        });

                    });
                });

            });

        });

        afterEach(function(done) {
            driver.quit().then(done);
        });

    }

    function getCommentNumber() {
        return new Promise(function(resolve, reject) {
            driver.findElement(selenium.By.id("side-comments-body"))
                .then(function(currrentPage) {
                    currrentPage.findElements(selenium.By.className("comment-wrapper"))
                        .then(function(commentViews) {
                            resolve(commentViews.length);
                        });
                });
        });
    }

    describe("sangsoo.lee (check has 2 comments (private(1), public(1))", function() {
        setup("sangsoo.lee@kewtea.com", "1234");
        it("has2comment", function(done) {
            getCommentNumber().then(function(number) {
                expect(number).toBe(2);
                done();
            });
        });
    });

    describe("lss6436 (check has 1 comment (private(1))", function() {
        setup("lss6436@gmail.com", "1234");
        it("has1comment", function(done) {
            getCommentNumber().then(function(number) {
                expect(number).toBe(1);
                done();
            });
        });
    });

});