

var SERVER_URL = 'http://localhost:8080';

// set access token
// 직접 curl 로 token 값을 받아와서 여기에 적어놓고 test run 할것.
// curl -d "grant_type=password&username=sangsoo.lee@kewtea.com&password=1234" -H "Authorization: Basic bXktdHJ1c3RlZC1jbGllbnQ6c2VjcmV0" http://localhost:8080/oauth/token
var token = "0d9d8e03-00bf-4875-ad63-758b94b80470";

var $ = require('jquery');
XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest;

describe("REST API default test", function() {

    /**
     * 1. GET
     * 2. POST (set inventory)
     * 3. DELETE (delete 2)inventory)
     */

    var get_status = -1;
    var post_status = -1;
    var delete_status = -1;

    var deleteId = -1; // post 에 사용했던 것 지우기

    describe("GET", function() {

        beforeEach(function(done) {

            var request = new XMLHttpRequest();
            request.open('GET', SERVER_URL+"/api/getusers", true);
            request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            request.onreadystatechange = function() {
                if (request.readyState == 4) {
                    get_status = request.status;
                    done();
                }
            };

            request.send(null);

        });

        it("check GET", function() {
            expect(get_status).toBe(200);
        });

    });

    describe("POST", function() {

        beforeEach(function(done) {

            var request = new XMLHttpRequest();
            request.open('POST', SERVER_URL+"/api/journal/collections?data={\"inventorytype\":\"collection\",\"authorid\":1,\"readaccess\":\"[1]\",\"writeaccess\":\"[1]\",\"name\":\"testCollection\",\"lastmodifiedtime\":1489574443188,\"createdtime\":1489574443188}", true);
            request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            request.setRequestHeader("Authorization", "Bearer "+token);
            request.onreadystatechange = function() {
                if (request.readyState == 4) {
                    post_status = request.status;
                    if (request.responseText) {
                        var response = JSON.parse(request.responseText);
                        deleteId = response.id;
                    }
                    done();
                }
            };

            request.send(null);

        });

        it("check POST", function() {
            expect(post_status).toBe(200);
        });

    });

    describe("DELETE", function() {

        beforeEach(function(done) {

            var request = new XMLHttpRequest();
            request.open('POST', SERVER_URL+"/api/journal/inventory?id="+deleteId, true);
            request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            request.setRequestHeader("Authorization", "Bearer "+token);
            request.onreadystatechange = function() {
                if (request.readyState == 4) {
                    delete_status = request.status;
                    done();
                }
            };

            request.send(null);

        });

        it("check DELETE", function() {
            expect(delete_status).toBe(200);
        });

    });

});