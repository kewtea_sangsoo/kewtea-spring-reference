function MockHttpService(networkStatus) {
    networkStatus = networkStatus || "office"; // "tunnel"

    function probability(n) {
        return !!n && Math.random() <= n;
    }

    var netWorkProbability = 1;
    var responseTime = 250;
    switch (networkStatus) {
        case "office" :
            responseTime = 250;
            netWorkProbability = 1;
        break;
        case "tunnel" :
            responseTime = 1000;
            netWorkProbability = 0.4;
        break;
    }

    var correctToken = "abc1234";

    var authToken;
    this.setAuthToken = function(token) {
        authToken = token;
    };

    var dummyData = [];
    dummyData.push({"id": 1, "text": "1st", "inventoryids": [1]});
    dummyData.push({"id": 2, "text": "2nd", "inventoryids": [1]});
    dummyData.push({"id": 3, "text": "3rd", "inventoryids": [2]});
    dummyData.push({"id": 4, "text": "4th", "inventoryids": [3]});
    dummyData.push({"id": 5, "text": "5th", "inventoryids": [4]});

    this.getAccessToken = function(refreshToken) {
        console.log("getAccessToken");
        return new Promise(function(resolve, reject) {
            setTimeout(function() {
                if (refreshToken == "abcd12345") {
                    resolve({
                        "access_token": correctToken,
                        "refresh_token": refreshToken
                    });
                } else {
                    reject({"data": {"error_description": "Invalid refresh token"}});
                }
            }, 1000);
        });
    };

    this.get = function(method, args) {
        return new Promise(function(resolve, reject) {
            switch (method) {

                case "commentsByInventoryId":
                    setTimeout(function() {
                        if (probability(netWorkProbability)) {
                            try {
                                if (authToken && correctToken != authToken) {
                                    // "Access token expired", "Invalid access token"
                                    reject({"data": {"error_description": "Access token expired"}});
                                }
                                var inventoryId = args["inventoryid"];
                                var fakeAnswer = [];
                                dummyData.forEach(function(data) {
                                    if (-1 < data.inventoryids.indexOf(inventoryId)) fakeAnswer.push(data);
                                });
                                resolve(fakeAnswer);
                            } catch (error) {
                                console.error(error);
                                reject(error);
                            }
                        } else {
                            reject();
                        }
                    }, responseTime);
                break;

                default: reject(); break;

            }
        });
    };


}

// Export node module.
if (typeof module !== 'undefined' && module.hasOwnProperty('exports')) module.exports = MockHttpService;