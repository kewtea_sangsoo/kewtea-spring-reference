/**
 * Created by sangsoolee on 2017. 1. 16..
 *
 * 로컬 캐시 저장소에, 데이터를 get/set(delete) 등을 하고 관리하는 function
 * - get 해봤는데, 데이터가 없는 경우 (null) -> null 이여도 resolve
 * - storage 에 I/O 기능 시도중에 오류가 일어난 경우에만, reject();
 */
function CacheManager() {
    var _cacheManager = this;

    var getStorageSize = function() {
        return new Promise(function(resolve, reject) {
            // resolve(Object.getOwnPropertyNames(_cacheManager.storage).length);
        });
    };

    this.get = function(key, withCacheInfo) {
        return new Promise(function(resolve, reject) {
            /*
            var data;
            try {

                if (!_cacheManager.storage.hasOwnProperty(key)) {
                    console.log("cache hit missed. key="+key);
                } else {
                    data = JSON.parse(_cacheManager.storage.getItem(key));
                }
                if (data && withCacheInfo !== true) {
                    delete data["_cacheInfo"];
                }
                resolve(data); // null 이여도 무조건 resolve

            } catch (reason) {
                // storage 에서 get 시도하다가 에러가 난 경우임 (I/O Error)
                reject(reason);
            }
            */
        });
    };

    this.set = function(key, value) {
        return new Promise(function(resolve, reject) {
            _cacheManager.onSetEvent(key, value);
            resolve();
            /*
            value = JSON.stringify(value);
            try {

                // cache 사용 안하는 옵션일 경우 (set 하지 않고 그냥 return)
                if (_cacheManager.maxSize == 0) return;

                // 동일한 데이터일 경우 set 안함
                var oldValue = _cacheManager.storage.getItem(key);
                if (oldValue) {
                    // 비교하기 전, 순수 데이터가 아닌 값들(accessTime, storedTime 등)을 떼어내고 순수한 데이터 끼리 비교하도록 한다
                    oldValue = JSON.parse(oldValue);
                    delete oldValue["_cacheInfo"];
                    oldValue = JSON.stringify(oldValue);
                }
                if (oldValue !== value) {

                    // TODO
                    // 추가 하기 전, maxSize 체크
                    if (_cacheManager.maxSize <= getStorageSize()) {
                        // 오래 된것 하나 지우기 (_lastAccessedTime)
                        // ...
                    }
                    //

                    // 추가 하기 전, 캐시 정보 추가 (언제 추가됬는가, 등)
                    value = JSON.parse(value);
                    value["_cacheInfo"] = {
                        savedTime: new Date().getTime()
                    };
                    value = JSON.stringify(value);
                    //

                    _cacheManager.storage.setItem(key, value);

                    // onSetEvent 할때는 _cacheInfo 가 필요없으므로 떼어내서 보내준다
                    value = JSON.parse(value);
                    delete value["_cacheInfo"];
                    _cacheManager.onSetEvent(key, value);
                    resolve(value);

                }

            } catch (reason) {
                reject(reason);
            }
            */
        });
    };

    this.delete = function(key) {
        return new Promise(function(resolve, reject) {
            /*
            try {
                _cacheManager.storage.removeItem(key);
                _cacheManager.onDeleteEvent(key);
                resolve();
            } catch (reason) {
                reject(reason);
            }
            */
        });
    };


    this.findAll = function(requestKey, withCacheInfo) {
        return new Promise(function(resolve, reject) {
            /*
            try {
                var array = [];
                for (var key in _cacheManager.storage) {
                    if (-1 < key.indexOf(requestKey)) {
                        var item = JSON.parse(_cacheManager.storage.getItem(key));
                        if (withCacheInfo !== true) {
                            delete item["_cacheInfo"];
                        }
                        array.push(item);
                    }
                }
                resolve(array);
            } catch (reason) {
                reject(reason);
            }
            */
        });
    };

}

// Export node module.
if (typeof module !== 'undefined' && module.hasOwnProperty('exports')) module.exports = CacheManager;