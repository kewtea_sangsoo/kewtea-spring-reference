/**
 *
 * 서버 -> 로컬에 저장 시, array 값이 string 으로 "[]" 이므로 -> JSON.parse 시켜서 진짜 array [] 로 만들어서 로컬에 저장
 * 로컬 -> 서버 전송 시, array 를 stringify 해서 "[...]" 로 만들어서 서버에 전송
 *
 * @constructor
 */
function DataParser() {

    // 서버 -> 로컬에 저장
    this.parseToLocal = function(model, data) {
        return data;
    };

    // 로컬 -> 서버로 전송
    this.parseToServer = function(model, data) {
        if (data) {
            switch (model) {

                case "content" :
                    data.readaccess = JSON.stringify(data.readaccess);
                    data.writeaccess = JSON.stringify(data.writeaccess);
                    if (data.contenttype == "chapterPage") data.body = JSON.stringify(data.body);
                    return data;
                    break;

                case "inventory" :
                    data.readaccess = JSON.stringify(data.readaccess);
                    data.writeaccess = JSON.stringify(data.writeaccess);
                    if (data["setting_value"]) data["setting_value"] = JSON.stringify(data["setting_value"]);
                    return data;
                    break;

                case "comment" :
                    data.readaccess = JSON.stringify(data.readaccess);
                    data.writeaccess = JSON.stringify(data.writeaccess);
                    data.contentids = JSON.stringify(data.contentids);
                    data.collectionids = JSON.stringify(data.collectionids);
                    data.keywordids = JSON.stringify(data.keywordids);
                    data.ranges = JSON.stringify(data.ranges);
                    data.highlights = JSON.stringify(data.highlights);
                    return data;
                    break;

                default:
                    console.warn("unavailable model type", model);

            }
        } else {
            console.warn("unavailable data", model, data);
        }
    }

}

// Export node module.
if (typeof module !== 'undefined' && module.hasOwnProperty('exports')) module.exports = DataParser;