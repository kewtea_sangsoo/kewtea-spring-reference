/**
 *
 * @constructor
 */
function UtilService() {
    this.getKeyBase = function(dataType) {
        return "k_" + dataType + "/";
    };
}

// Export node module.
if (typeof module !== 'undefined' && module.hasOwnProperty('exports')) module.exports = UtilService;