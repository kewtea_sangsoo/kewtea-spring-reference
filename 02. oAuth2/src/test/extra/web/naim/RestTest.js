var XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest;

var SERVER_URL = 'http://localhost:8080';

// set access token
// 직접 curl 로 token 값을 받아와서 여기에 적어놓고 test run 할것.
// curl -d "grant_type=password&username=sangsoo.lee@kewtea.com&password=1234" -H "Authorization: Basic bXktdHJ1c3RlZC1jbGllbnQ6c2VjcmV0" http://localhost:8080/oauth/token
var token = '07a61b8f-f610-4eaa-aa59-48b06bc991cc'; // access token 값을 여기에 적으시오

var testURLs = {
    'getContentsByIds': {
        'methodType': 'GET',
        'requestUrl': SERVER_URL+'/api/naim/contents?ids=1,2,3',
    },
    'getThemesByUserId': {
        'methodType': 'GET',
        'requestUrl': SERVER_URL+'/api/naim/contents?type=theme&userid=1',
    },
    'getChannelsByUserId': {
        'methodType': 'GET',
        'requestUrl': SERVER_URL+'/api/naim/contents?type=channelPlan&userid=1',
    },
    'getBlocksByUserId': {
        'methodType': 'GET',
        'requestUrl': SERVER_URL+'/api/naim/contents?type=block&userid=1',
    },
    'getScenesByUserId': {
        'methodType': 'GET',
        'requestUrl': SERVER_URL+'/api/naim/contents?type=scene&userid=1',
    },
    'getDevicesByUserId': {
        'methodType': 'GET',
        'requestUrl': SERVER_URL+'/api/naim/inventory?type=device&userid=1',
    },
    'getDeviceGroupsByUserId': {
        'methodType': 'GET',
        'requestUrl': SERVER_URL+'/api/naim/inventory?type=deviceGroup&userid=1',
    },
};

describe('REST API Test', function() {

    function BuildRequest(type, url, body, callback) {
        var timeOffset = -new  Date().getTimezoneOffset().toString();
        var request = new XMLHttpRequest();
            request.open(type, url, true);
            request.setRequestHeader('Content-Type', 'application/json');
            request.setRequestHeader('Authorization', 'Bearer '+token);
            request.setRequestHeader('TimeOffset', timeOffset);
            request.onreadystatechange = function() {
                if (request.readyState == 4) {
                    var response = null;
                    callback(request.status);
                }
            };
        return request;
    }

    for (var key in testURLs) {
        if (Object.prototype.hasOwnProperty.call(testURLs, key) ||
            {}.hasOwnProperty.call(testURLs, key)) {
            var testURL = testURLs[key];
            describe(testURL, function() {
                var responseStatus = -1;
                beforeEach(function(done) {
                    var request = new BuildRequest(
                        testURL['methodType'],
                        testURL['requestUrl'],
                        testURL['requestBody'],
                        function(status) {
                            responseStatus = status;
                            done();
                        }
                    );
                    request.send(null);
                });
                it('response is OK', function() {
                    expect(responseStatus).toBe(200);
                });
            });
        }
    }

});
