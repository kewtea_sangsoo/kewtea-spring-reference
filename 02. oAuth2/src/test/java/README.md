여기 test case 들은 크게 
일반 JUnit test 와, spring/ 디렉토리 밑에 있는 spring integration test 로 나뉩니다.

JUnit test 로 기본적인 기능들을 test 하여 구현하고,
spring integration test 로 복잡한 시나리오 등을 테스트 해봅니다. 
