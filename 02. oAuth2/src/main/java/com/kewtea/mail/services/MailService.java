package com.kewtea.mail.services;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.mailjet.client.MailjetClient;
import com.mailjet.client.MailjetRequest;
import com.mailjet.client.errors.MailjetException;
import com.mailjet.client.errors.MailjetSocketTimeoutException;
import com.mailjet.client.resource.Email;

@Service
public class MailService {

	private TemplateEngine templateEngine;
	@Autowired
	public MailService(TemplateEngine templateEngine) {
		this.templateEngine = templateEngine;
	}

	//

	@Async
	public void send(String to, String subject, String body) throws MailjetException, MailjetSocketTimeoutException {
		JSONArray recipients = new JSONArray();
		JSONObject recipient = new JSONObject().put("Email", to);
		recipients.put(recipient);
		
		MailjetClient client = new MailjetClient("ec22d0448a4deec21eeb96f05bac3b97","db45db63024ab4cd43fa909817ac7424");
		MailjetRequest request = new MailjetRequest(Email.resource)
									.property(Email.FROMEMAIL, "admin@kewtea.com")
									.property(Email.FROMNAME, "Kewtea")
									.property(Email.SUBJECT, subject)
									.property(Email.HTMLPART, body)
									.property(Email.RECIPIENTS, recipients);
		
//		MailjetResponse response = client.post(request);
		client.post(request);
	}

	public String buildBodyByTemplate(String templateName, HashMap<String, Object> msgs) {
		Context context = new Context();
		for (Map.Entry<String, Object> msg : msgs.entrySet()) {
			String key = msg.getKey();
		    Object value = msg.getValue();
		    context.setVariable(key, value);
		}
		return templateEngine.process(templateName, context);
	}

}
