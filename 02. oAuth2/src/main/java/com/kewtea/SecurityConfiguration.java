package com.kewtea;

import java.util.Collection;
import java.util.Iterator;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.code.AuthorizationCodeServices;
import org.springframework.security.oauth2.provider.code.JdbcAuthorizationCodeServices;
import org.springframework.security.oauth2.provider.error.DefaultWebResponseExceptionTranslator;
import org.springframework.security.oauth2.provider.error.WebResponseExceptionTranslator;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.kewtea.user.LoginFailureHandler;
import com.kewtea.user.LoginSuccessHandler;
import com.kewtea.user.services.KewUserDetailsService;

// TODO web configure, http configure 한 곳에서 처리 가능할 것 같음..  doc 좀더 읽어보자
// https://docs.spring.io/spring-security/site/docs/4.2.4.RELEASE/apidocs/org/springframework/security/config/annotation/web/configuration/WebSecurityConfiguration.html
// https://docs.spring.io/spring-security/oauth/apidocs/org/springframework/security/oauth2/config/annotation/web/configuration/ResourceServerConfiguration.html
@Configuration
public class SecurityConfiguration {

	@Value("${spring.rememberMeDomain}")
	public void setRememberMeDomain(String rememberMeDomain) {
		SecurityConfiguration.rememberMeDomain = rememberMeDomain;
	}

	private static String RESOURCE_ID = "restservice";
	private static String rememberMeDomain;
	private static final Logger log = Logger.getLogger(SecurityConfiguration.class.getName());

	//

	@Configuration
	@EnableWebSecurity
	protected static class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

		@Configuration
		static class AuthenticationConfiguration extends GlobalAuthenticationConfigurerAdapter {
			@Autowired
			KewUserDetailsService userDetailsService;
			@Bean
			PasswordEncoder passwordEncoder() {
				return new BCryptPasswordEncoder();
			}
			@Override
			public void init(AuthenticationManagerBuilder auth) throws Exception {
				auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
			}
		}

		@Override
		public void configure(WebSecurity web) throws Exception {
			web.ignoring()
				.antMatchers("/lib/**")
				.antMatchers("/kewtea/css/**")
				.antMatchers("/kewtea/lib/**")
				.antMatchers("/kewtea/img/**")
				.antMatchers("/kewtea/video/**")
				.antMatchers("/journal/img/**")
				.antMatchers("/journal/lib/**")
				.antMatchers("/journal/js/**")
				.antMatchers("/journal/css/**")
				.antMatchers("/journal/product/**")
				.antMatchers("/naim/css/**")
				.antMatchers("/naim/js/**")
				.antMatchers("/naim/img/**")
				.antMatchers("/naim/extensions/**")
				.antMatchers("/naim/player/css/**")
				.antMatchers("/naim/player/js/**");
		}

		@Override
		@Bean
		public AuthenticationManager authenticationManagerBean() throws Exception {
			return super.authenticationManagerBean();
		}
	}

	@Configuration
	@EnableAuthorizationServer
	protected static class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {

		@Autowired
	    private DataSource dataSource;

		@Autowired
		@Qualifier("authenticationManagerBean")
		private AuthenticationManager authenticationManager;

		@Bean
		public JdbcTokenStore tokenStore() {
			return new JdbcTokenStore(dataSource);
		}

		@Bean
		protected AuthorizationCodeServices authorizationCodeServices() {
			return new JdbcAuthorizationCodeServices(dataSource);
		}

		@Autowired
		private KewUserDetailsService userDetailsService;

		// oAuth authentication
		@Override
		public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
			endpoints.authorizationCodeServices(this.authorizationCodeServices())
				.exceptionTranslator(webResponseExceptionTranslator(this.tokenStore()))
				.tokenStore(this.tokenStore())
				.authenticationManager(this.authenticationManager)
				.userDetailsService(this.userDetailsService)
				.approvalStoreDisabled();
		}

		@Override
		public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
			clients.jdbc(dataSource);
		}

		@Bean
		@Primary
		public DefaultTokenServices tokenServices() {
			DefaultTokenServices tokenServices = new DefaultTokenServices();
			tokenServices.setSupportRefreshToken(true);
			tokenServices.setTokenStore(this.tokenStore());
			return tokenServices;
		}

		// https://github.com/spring-projects/spring-security-oauth/issues/665
		// http://stackoverflow.com/questions/32912460/how-to-inject-webresponseexceptiontranslator-in-tokenendpoint-spring-oauth2
		@Bean
		public WebResponseExceptionTranslator webResponseExceptionTranslator(JdbcTokenStore tokenStore) {

			return new DefaultWebResponseExceptionTranslator() {

				@Override
				public ResponseEntity<OAuth2Exception> translate(Exception e) {
					try {
						log.info("===== authExcpetionHandler ====");
						String errorMsg = e.getMessage();
						e.printStackTrace();

						ResponseEntity<OAuth2Exception> responseEntity = super.translate(e);
			            OAuth2Exception body = responseEntity.getBody();
			            HttpHeaders headers = new HttpHeaders();
			            headers.setAll(responseEntity.getHeaders().toSingleValueMap());

			            // do something ...
			            if (errorMsg == null) {
			            		Collection<OAuth2AccessToken> tokens = tokenStore.findTokensByClientId("my-trusted-client");
				            	Iterator<OAuth2AccessToken> itr = tokens.iterator();
				            	while (itr.hasNext()) {
				            		OAuth2AccessToken token = itr.next();
				            		OAuth2RefreshToken refreshToken = token.getRefreshToken();
				            		tokenStore.removeAccessToken(token);
				            		tokenStore.removeRefreshToken(refreshToken);
			            		}
		            		}

			            return new ResponseEntity<>(body, headers, responseEntity.getStatusCode());

					} catch (Exception ex) {
						ex.printStackTrace();
						return null;
					}
		         }

		     };
		 }

	}

	@Configuration
	@EnableResourceServer
	protected static class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

		@Autowired
		LoginSuccessHandler successHandler;
		@Autowired
		LoginFailureHandler failureHandler;
		@Autowired
		Environment environment;

		@Override
		public void configure(ResourceServerSecurityConfigurer resources) {
			resources.resourceId(RESOURCE_ID);
		}

		@Override
		public void configure(HttpSecurity http) throws Exception {
			// cross domain problem
			http.csrf().disable();

			http.authorizeRequests()
					.antMatchers("/kewtea/dashboard/**").hasRole("SUPERADMIN")
					.anyRequest().permitAll(); // 모든 http 에 header 값에 인증 정보를 실어서 보내도록 함 (막는 것이 아님)

			http.formLogin()
					.loginPage("/login")
					.loginProcessingUrl("/dologin")
					.usernameParameter("username").passwordParameter("password")
					.successHandler(successHandler)
					.failureHandler(failureHandler)
					.permitAll();

			http.rememberMe()
					.key("rem-me-key")
		            .rememberMeParameter("remember-me-param")
		            .rememberMeCookieName("my-remember-me")
		            .rememberMeCookieDomain(rememberMeDomain)
		            .tokenValiditySeconds(63072000); // 2 years 60*60*24*365*2 = 63072000

			http.logout()
					.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
					.logoutSuccessUrl("/login");

			http.headers().frameOptions().disable();

		}


	}


}
