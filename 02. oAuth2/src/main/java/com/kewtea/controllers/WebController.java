package com.kewtea.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.kewtea.user.services.KewUserService;
import com.google.gson.Gson;
import com.kewtea.user.MyUserDetails;
import com.kewtea.user.models.User;

@Controller
@RequestMapping("/")
public class WebController {
	
	@Value("${spring.serverAddress}")
	private String serverAddress;
	
	@Autowired
	private KewUserService userService;
	
	Gson gson = new Gson();
	
	//
	
	private Model buildModel(Model model) {
		model.addAttribute("serverAddress", serverAddress);
		return model;
	}
	
	@RequestMapping("/login")
	String login(Model model) {
		model = buildModel(model);
		return "login";
	}

	@RequestMapping(value="/signup")
	String signupPage(Model model) {
		model = buildModel(model);
		return "signup";
	}
	
	@RequestMapping(value="/signedup")
	String signedupPage(Model model) {
		model = buildModel(model);

		model.addAttribute("loginUrl", serverAddress+"/login");
		model.addAttribute("text", "Please check your email inbox.");

		return "checkemail";
	}
	
	@RequestMapping(value="/activateuser")
	String activeUser(Model model, @RequestParam(value="code") String code) {
		model = buildModel(model);
		try {
			userService.activateUser(code);
			return "redirect:"+serverAddress+"/login";
		} catch (Exception e) {
			// TODO : expired 일 경우, re-send 버튼이 달려있는 폼 제공
			// ... if is expired ...
			model.addAttribute("exceptionTitle", e.getMessage());
			model.addAttribute("goToText", "go to kewtea.com");
			return "error";
		}
	}
	
	//
	
	@RequestMapping(value="/login_success")
	String loginSuccessPage(Model model, @AuthenticationPrincipal MyUserDetails userDetails) {
		model = buildModel(model);
		
		User currentUser = userService.getUserByEmail(userDetails.getUsername());
		model.addAttribute("currentUserJson", gson.toJson(currentUser));
		
		return "login_success";
	}

}
