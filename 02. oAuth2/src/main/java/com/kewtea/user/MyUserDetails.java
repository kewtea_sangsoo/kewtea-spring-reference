package com.kewtea.user;

import org.springframework.security.core.GrantedAuthority;

import com.kewtea.user.models.User;

import lombok.Data;


@Data
public class MyUserDetails extends org.springframework.security.core.userdetails.User {

	private static final long serialVersionUID = 4404018321491222158L;
	private final User user;	
	
	public MyUserDetails(User user, java.util.Collection<? extends GrantedAuthority> authorities) {
		super(user.email, user.password, authorities); //여기도 나중에 로직을 넣읍시다
		this.user = user;  // 먼저 construction을 하고 객체를 할당해야한다
	}
	
	@Override
	public boolean isEnabled() { return user.active; }
	@Override
	public boolean isAccountNonExpired() { return user.active; }
	@Override
	public boolean isAccountNonLocked() { return user.active; }
	@Override
	public boolean isCredentialsNonExpired() { return user.active; }
	
	public long getId() {
		return this.user.id;
	}

}