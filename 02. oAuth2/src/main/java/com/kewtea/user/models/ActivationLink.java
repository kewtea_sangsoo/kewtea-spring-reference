package com.kewtea.user.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "activationlinks")
public class ActivationLink {

	// constructor
	public ActivationLink() {}
	public ActivationLink(long userId, String url, long createdTime) {
		this.userid = userId;
		this.url = url;
		this.createdtime = createdTime;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long id;

	@Column(nullable = true)
	public long userid;

	@Column(nullable = true)
	public String url;

	@Column(nullable = true)
	public long createdtime;

	// -1: ready for activate
	// 0: expire (시간이 만료된게 아닌, 같은 uid 로 재발행 된 경우)
	// 1: activated (정상 사용 완료)
	@Column
	public long status = -1L;

}
