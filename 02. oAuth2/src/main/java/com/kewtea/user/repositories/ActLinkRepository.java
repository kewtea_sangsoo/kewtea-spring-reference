package com.kewtea.user.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.kewtea.user.models.ActivationLink;

@Repository
public interface ActLinkRepository extends JpaRepository<ActivationLink, Long> {

	// by url
	public ActivationLink findByUrl(String url);

	// by user
	public List<ActivationLink> findByUserid(long userid);

	// find old ActLinks
	public List<ActivationLink> findByCreatedtimeLessThan(long createdtime);

}
