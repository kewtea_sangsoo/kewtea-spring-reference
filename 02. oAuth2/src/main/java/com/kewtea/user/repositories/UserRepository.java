package com.kewtea.user.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.kewtea.user.models.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	// by id
	User findById(long id);
	
	// by email
	User findByEmail(String email);

}
